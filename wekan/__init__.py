# coding=utf-8

"""Package to collect Wekan metrics."""

from .wekan import WekanCollector
