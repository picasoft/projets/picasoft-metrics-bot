# coding=utf-8

"""Functions to export Mattermost metrics."""

# Libs
import json
import datetime
from urllib.parse import urlparse
from mattermostdriver import Driver


class MattermostCollector():
    """
    MattermostCollector.
    Collector for Mattermost stats.
    """

    def __init__(self, config):
        """
        Initialize a mattermost collector object.
        :param config: Configuration for Mattermost module (list of instances)
        """
        # Initialize list of instances connector
        self.instances = []
        for instance in config:
            if 'url' not in instance or 'name' not in instance or 'user' not in instance or 'password' not in instance:
                print('Incorrect instance configuration\n')
                print(instance)
                print('"mattermost" key on configuration file should be a list of object with "url", "user", "password" and "name" attributes')
                continue
            else:
                # Parse the URL
                o = urlparse(instance['url'])
                if o.port is None:
                    if o.scheme == 'https':
                        mm_port = 443
                    elif o.scheme == 'http':
                        mm_port = 80
                    else:
                        print('Impossible to get Mattermost server port for ' + instance['url'])
                        continue
                else:
                    mm_port = o.port

                # Connect to mattermost
                mat_api = Driver({
                    'url': o.hostname,
                    'login_id': instance['user'],
                    'password': instance['password'],
                    'scheme': o.scheme,
                    'port': mm_port
                })
                try:
                    mat_api.login()
                except Exception as err:
                    print("Cannot login to {} : {}".format(instance['name'], err))
                    continue
                self.instances.append(
                    {
                        'conn': mat_api,
                        'config': instance
                    }
                )

    def collect(self):
        """
        Get the analytics of mattermost instances and returns a list of InfluxDB points.
        :returns: List of InfluxDB formatted objects
        """
        metrics = []

        # Get all instances stats
        for instance in self.instances:
            # Get current stats
            print("Mattermost : collecting for instance {}".format(instance['config']['name']))
            data = self._get_stats(instance)
            if data is None:
                print('Unable to get stats from Mattermost instance ' + instance['config']['name'])
                continue
            # Get current miliseconds timestamp
            current_timestamp = int(datetime.datetime.now().timestamp()*1000)
            # Create metrics
            metrics.append({
                'measurement': 'mattermost_public_channels_count',
                'tags': {
                    'name': instance['config']['name']
                },
                'time': current_timestamp,
                'fields': {
                    'value': data['public_channels']
                }
            })
            metrics.append({
                'measurement': 'mattermost_private_channels_count',
                'tags': {
                    'name': instance['config']['name']
                },
                'time': current_timestamp,
                'fields': {
                    'value': data['private_channels']
                }
            })
            metrics.append({
                'measurement': 'mattermost_posts_count',
                'tags': {
                    'name': instance['config']['name']
                },
                'time': current_timestamp,
                'fields': {
                    'value': data['posts']
                }
            })
            metrics.append({
                'measurement': 'mattermost_users_count',
                'tags': {
                    'name': instance['config']['name']
                },
                'time': current_timestamp,
                'fields': {
                    'value': data['users']
                }
            })
            metrics.append({
                'measurement': 'mattermost_teams_count',
                'tags': {
                    'name': instance['config']['name']
                },
                'time': current_timestamp,
                'fields': {
                    'value': data['teams']
                }
            })

            metrics.append({
                'measurement': 'total_websocket_connections',
                'tags': {
                    'name': instance['config']['name']
                },
                'time': current_timestamp,
                'fields': {
                    'value': data['total_websocket_connections']
                }
            })

            # Get daily stats
            daily_data = self._get_daily_stats(instance)
            if daily_data is None:
                print('Unable to get daily stats from Mattermost instance ' + instance['config']['url'])
                continue

            # Create metrics
            metrics.append({
                'measurement': 'mattermost_daily_posts',
                'tags': {
                    'name': instance['config']['name']
                },
                'time': daily_data['daily_posts']['timestamp'],
                'fields': {
                    'value': daily_data['daily_posts']['value']
                }
            })
            metrics.append({
                'measurement': 'mattermost_daily_users',
                'tags': {
                    'name': instance['config']['name']
                },
                'time': daily_data['daily_users_with_posts']['timestamp'],
                'fields': {
                    'value': daily_data['daily_users_with_posts']['value']
                }
            })
            print("Mattermost : data collected for instance {}".format(instance['config']['name']))
        return metrics

    @classmethod
    def _get_stats(cls, instance):
        """
        Get stats for a Mattermost instance.
        :param instance: Object with configuration and API connector for Mattermost instance
        :returns: JSON data returned by Mattermost API
        """
        # Get count stats
        data = {}
        try:
            res = json.loads(instance['conn'].client.make_request('get', '/analytics/old', params={'name': 'standard'}).text)
        except Exception as err:
            print(err)
            return None
        # Extract values
        data['public_channels'] = cls._get_value(res, 'channel_open_count')
        data['private_channels'] = cls._get_value(res, 'channel_private_count')
        data['total_websocket_connections'] = cls._get_value(res, 'total_websocket_connections')
        data['posts'] = cls._get_value(res, 'post_count')
        data['users'] = cls._get_value(res, 'unique_user_count')
        data['teams'] = cls._get_value(res, 'team_count')
        return data

    @classmethod
    def _get_daily_stats(cls, instance):
        """
        Get daily stats for a Mattermost instance.
        :param instance: Object with configuration and API connector for Mattermost instance
        :returns: JSON data returned by Mattermost API
        """
        # Get data
        data = {}
        try:
            posts = json.loads(instance['conn'].client.make_request('get', '/analytics/old', params={'name': 'post_counts_day'}).text)
            users = json.loads(instance['conn'].client.make_request('get', '/analytics/old', params={'name': 'user_counts_with_posts_day'}).text)
        except Exception as err:
            print(err)
            return None

        # Extract values
        data['daily_posts'] = {
            'value': posts[0]['value'],
            'timestamp': int(datetime.datetime.strptime(posts[0]['name'], '%Y-%m-%d').timestamp()*1000)
        }
        data['daily_users_with_posts'] = {
            'value': users[0]['value'],
            'timestamp': int(datetime.datetime.strptime(users[0]['name'], '%Y-%m-%d').timestamp()*1000)
        }
        return data

    @classmethod
    def _get_value(cls, data, key):
        """
        Get the value for a specific key in Mattermost analytics data
        :param data: Mattermost data
        :param key: Key to search
        :returns: Value for the specific key
        """
        try:
            res = (i for i in data if i['name'] == key).__next__()
        except StopIteration:
            return None
        return res['value']
