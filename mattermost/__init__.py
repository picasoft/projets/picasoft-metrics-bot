# coding=utf-8

"""Package to collect Mattermost metrics."""

from .mattermost import MattermostCollector
