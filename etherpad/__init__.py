# coding=utf-8

"""Package to collect Etherpad metrics."""

from .etherpad import EtherpadCollector
