# coding=utf-8

"""Functions to export Etherpad metrics."""

# Libs
import json
import requests


class EtherpadCollector(object):
    """
    EtherpadCollector.
    Collector for Etherpad stats.
    """

    def __init__(self, config):
        """
        Initialize an etherpad collector object.
        :param config: Configuration for Etherpad module (list of instances)
        """
        # Initialize list of instances based on configuration
        self.instances = []
        for instance in config:
            if 'url' not in instance or 'name' not in instance:
                print('Incorrect instance configuration\n')
                print(instance)
                print('"etherpad" key on configuration file should be a list of object with "url" and "name" attributes')
                continue
            else:
                if instance['url'].endswith('/'):
                    instance['url'] = instance['url'][:-1]
                self.instances.append(instance)

    def collect(self):
        """
        Get the analytics of etherpad instances and returns a list of InfluxDB points.
        :returns: List of InfluxDB formatted objects
        """
        metrics = []

        # Get all instances stats
        for instance in self.instances:
            print("Etherpad : collecting for instance {}".format(instance['name']))
            data = self._get_stats(instance)
            if data is None:
                continue
            # Create metrics
            metrics.append({
                'measurement': 'etherpad_pads_count',
                'tags': {
                    'name': instance['name']
                },
                'time': data['timestamp']*1000,
                'fields': {
                    'value': data['padsCount']
                }
            })
            metrics.append({
                'measurement': 'etherpad_blank_pads_count',
                'tags': {
                    'name': instance['name']
                },
                'time': data['timestamp']*1000,
                'fields': {
                    'value': data['blankPads']
                }
            })
            metrics.append({
                'measurement': 'etherpad_total_users',
                'tags': {
                    'name': instance['name']
                },
                'time': data['timestamp']*1000,
                'fields': {
                    'value': data['totalUsers']
                }
            })
            print("Etherpad : data collected for instance {}".format(instance['name']))

        return metrics

    @classmethod
    def _get_stats(cls, instance):
        """
        Get stats for an Etherpad instance.
        :param instance: Configuration for Etherpad instance (dict with at least "url" key)
        :returns: JSON data returned by Etherpad stats module
        """
        # Send request
        try:
            stats_plugin = requests.get(instance['url'] + "/stats.json")
            stats_native = requests.get(instance['url'] + "/stats")
            if stats_plugin.status_code != 200:
                print("Unable to get stats.json from {} : HTTP {}".format(instance['name'], stats_plugin.status_code))
                return None
            if stats_native.status_code != 200:
                print("Unable to get stats from {} : HTTP {}".format(instance['name'], stats_plugin.status_code))
                return None
            try:
                plugin = json.loads(stats_plugin.text)
                native = json.loads(stats_native.text)
                plugin['totalUsers'] = native['totalUsers']
                return plugin
            except IOError as err:
                print(err)
                return None
        except requests.exceptions.RequestException as e:
            print('Unable to contact Etherpad : {}'.format(e))
